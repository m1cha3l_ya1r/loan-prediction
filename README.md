This is a practice problem from [Analytics Vidhya](https://datahack.analyticsvidhya.com/contest/practice-problem-loan-prediction-iii/)

I got it as a final assignment in my course (2018) and found the source only recently.

You may also find the a [Kaggle notebook](https://www.kaggle.com/ufffnick/loan-prediction-dream-housing-finance) version of this code, upvoted and forked.

You might notice that I got the highest prediction stats whithin the codes uploaded to Kaggle.
